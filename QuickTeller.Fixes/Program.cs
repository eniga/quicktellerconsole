﻿using Microsoft.Extensions.Configuration;
using System;

namespace QuickTeller.Fixes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        static void StartUp()
        {
             IConfiguration configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.Development.json")
            .AddJsonFile("appsettings.json")
            .Build();
        }
    }
}
