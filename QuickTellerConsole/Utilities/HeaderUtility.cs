﻿using System;
using System.Configuration;
using System.Text;
using QuickTellerConsole.Models;

namespace QuickTellerConsole.Utilities
{
    public static class HeaderUtility
    {
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }


        public static APIHeader GenerateAPIHeader(string http_verb, string url, string encryptionType)
        {
            string ClientID = ConfigurationManager.AppSettings["ClientID"];
            string Secret = ConfigurationManager.AppSettings["Secret"];
            string Nonce = Guid.NewGuid().ToString().Replace("-", "") + Helper.GenerateRandomNumber(10);
            var timestamp = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds.ToString().Substring(0, 10);

            string SignatureInput = http_verb + "&" + Uri.EscapeDataString(url) + "&" + timestamp + "&" + Nonce + "&" + ClientID + "&" + Secret;
            string encryptedSignatureInput = string.Empty;
            switch (encryptionType.ToUpper())
            {
                case "SHA1":
                    encryptedSignatureInput = SHAUtility.SHA1Hash(SignatureInput);
                    break;
                case "SHA256":
                    encryptedSignatureInput = SHAUtility.SHA256Hash(SignatureInput);
                    break;
                case "SHA512":
                    encryptedSignatureInput = SHAUtility.SHA512Hash(SignatureInput);
                    break;
            }

            APIHeader result = new APIHeader()
            {
                Authorization = Base64Encode(ClientID),
                Signature = encryptedSignatureInput,
                SignatureMethod = encryptionType.ToUpper(),
                Timestamp = timestamp,
                Nonce = Nonce,
                ClientID = ConfigurationManager.AppSettings["ClientID"],
                RequestReferencePrefix = ConfigurationManager.AppSettings["RequestReferencePrefix"],
                Secret = ConfigurationManager.AppSettings["Secret"],
                TerminalID = ConfigurationManager.AppSettings["TerminalID"]
            };
            return result;
        }

    }
}
