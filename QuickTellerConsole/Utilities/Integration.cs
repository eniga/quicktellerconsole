﻿using System;
using System.Configuration;
using System.Net;
using System.Threading.Tasks;
using QuickTellerConsole.Models;
using RestSharp;

namespace QuickTellerConsole.Utilities
{
    public static class Integration
    {
        public static BillersResponse GetBillers()
        {
            BillersResponse response = new BillersResponse();
            string baseUrl = ConfigurationManager.AppSettings["baseUrl"];
            string ApiUrl = "api/v2/quickteller/billers";

            RestClient client = new RestClient(baseUrl);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var header = HeaderUtility.GenerateAPIHeader("GET", baseUrl + ApiUrl, "SHA1");

            var request = new RestRequest(ApiUrl, method: Method.GET);

            request.AddHeader("Authorization", "InterswitchAuth " + header.Authorization);
            request.AddHeader("Signature", header.Signature);
            request.AddHeader("SignatureMethod", header.SignatureMethod);
            request.AddHeader("Timestamp", header.Timestamp);
            request.AddHeader("TerminalID", header.TerminalID);
            request.AddHeader("Nonce", header.Nonce);

            var result = client.Execute<BillersResponse>(request);
            if (result.IsSuccessful)
            {
                response = result.Data;
            }

            return response;
        }

        public static PaymentItemsResponse GetBillerPaymentItems(string billerId)
        {
            PaymentItemsResponse response = new PaymentItemsResponse();
            string baseUrl = ConfigurationManager.AppSettings["baseUrl"];
            string ApiUrl = $"api/v2/quickteller/billers/{billerId}/paymentitems";

            RestClient client = new RestClient(baseUrl);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var header = HeaderUtility.GenerateAPIHeader("GET", baseUrl + ApiUrl, "SHA1");

            var request = new RestRequest(ApiUrl, method: Method.GET);

            request.AddHeader("Authorization", "InterswitchAuth " + header.Authorization);
            request.AddHeader("Signature", header.Signature);
            request.AddHeader("SignatureMethod", header.SignatureMethod);
            request.AddHeader("Timestamp", header.Timestamp);
            request.AddHeader("TerminalID", header.TerminalID);
            request.AddHeader("Nonce", header.Nonce);

            var result = client.Execute<PaymentItemsResponse>(request);
            if (result.IsSuccessful)
            {
                response = result.Data;
            }

            return response;
        }

        public static CategoryResponse GetBillerCategories()
        {
            string baseUrl = ConfigurationManager.AppSettings["baseUrl"];
            string ApiUrl = "api/v2/quickteller/categorys";
            CategoryResponse response = new CategoryResponse();

            RestClient client = new RestClient(baseUrl);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var header = HeaderUtility.GenerateAPIHeader("GET", baseUrl + ApiUrl, "SHA1");

            var request = new RestRequest(ApiUrl, method: Method.GET);

            request.AddHeader("Authorization", "InterswitchAuth " + header.Authorization);
            request.AddHeader("Signature", header.Signature);
            request.AddHeader("SignatureMethod", header.SignatureMethod);
            request.AddHeader("Timestamp", header.Timestamp);
            request.AddHeader("TerminalID", header.TerminalID);
            request.AddHeader("Nonce", header.Nonce);

            var result = client.Execute<CategoryResponse>(request);
            if (result.IsSuccessful)
            {
                response = result.Data;
            }

            return response;
        }

        public static BillersResponse GetBillersByCategory(int id)
        {
            BillersResponse response = new BillersResponse();
            string baseUrl = ConfigurationManager.AppSettings["baseUrl"];
            string ApiUrl = $"api/v2/quickteller/categorys/{id}/billers";

            RestClient client = new RestClient(baseUrl);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var header = HeaderUtility.GenerateAPIHeader("GET", baseUrl + ApiUrl, "SHA1");

            var request = new RestRequest(ApiUrl, method: Method.GET);

            request.AddHeader("Authorization", "InterswitchAuth " + header.Authorization);
            request.AddHeader("Signature", header.Signature);
            request.AddHeader("SignatureMethod", header.SignatureMethod);
            request.AddHeader("Timestamp", header.Timestamp);
            request.AddHeader("TerminalID", header.TerminalID);
            request.AddHeader("Nonce", header.Nonce);

            var result = client.Execute<BillersResponse>(request);
            if (result.IsSuccessful)
            {
                response = result.Data;
            }

            return response;
        }

        public static BillPaymentNotificationResponse QueryTransaction(string RequestReference)
        {
            BillPaymentNotificationResponse response = new BillPaymentNotificationResponse();
            RequestReference = ConfigurationManager.AppSettings["RequestReferencePrefix"] + RequestReference;
            string baseUrl = ConfigurationManager.AppSettings["baseUrl"];
            string ApiUrl = $"api/v2/quickteller/transactions?requestreference={RequestReference}";

            RestClient client = new RestClient(baseUrl);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var header = HeaderUtility.GenerateAPIHeader("GET", baseUrl + ApiUrl, "SHA1");

            var request = new RestRequest(ApiUrl, method: Method.GET);

            request.AddHeader("Authorization", "InterswitchAuth " + header.Authorization);
            request.AddHeader("Signature", header.Signature);
            request.AddHeader("SignatureMethod", header.SignatureMethod);
            request.AddHeader("Timestamp", header.Timestamp);
            request.AddHeader("TerminalID", header.TerminalID);
            request.AddHeader("Nonce", header.Nonce);

            var result = client.Execute<TransactionDetails>(request);
            if (result.IsSuccessful)
            {
                var qtResp = DbConnect.GetQTResponseAsync(result.Data.transactionResponseCode).Result;
                response.Status = qtResp.Status;
                response.StatusMessage = qtResp.Description;
                response.transactionRef = result.Data.transactionRef;
            }

            return response;
        }

        public static BillPaymentInquiryResponse BillPaymentInquiry(BillPaymentInquiryRequest req)
        {
            BillPaymentInquiryResponse response = new BillPaymentInquiryResponse();
            string baseUrl = ConfigurationManager.AppSettings["baseUrl"];
            string ApiUrl = $"api/v2/quickteller/transactions/inquirys";

            RestClient client = new RestClient(baseUrl);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var header = HeaderUtility.GenerateAPIHeader("GET", baseUrl + ApiUrl, "SHA1");

            var request = new RestRequest(ApiUrl, method: Method.GET);

            request.AddHeader("Authorization", "InterswitchAuth " + header.Authorization);
            request.AddHeader("Signature", header.Signature);
            request.AddHeader("SignatureMethod", header.SignatureMethod);
            request.AddHeader("Timestamp", header.Timestamp);
            request.AddHeader("TerminalID", header.TerminalID);
            request.AddHeader("Nonce", header.Nonce);

            request.AddJsonBody(req);

            var result = client.Execute<BillPaymentInquiryResponse>(request);
            if (result.IsSuccessful)
            {
                response = result.Data;
            }

            return response;
        }
        /*
        public static BillPaymentNotificationResponse SendBillPaymentNotification(SendBillPaymentNotificationRequest request)
        {
            BillPaymentNotificationResponse response = new BillPaymentNotificationResponse();
            string baseUrl = ConfigurationManager.AppSettings["baseUrl"];
            string ApiUrl = $"api/v2/quickteller/payments/advices";

            try
            {
                // Check if request reference exists
                request.requestReference = ConfigurationManager.AppSettings["RequestReferencePrefix"] + request.requestReference;
                var exists = DbConnect.BillPaymentRequestExists(request.requestReference);
                if (exists)
                {
                    response.Status = false;
                    response.StatusMessage = "Request Reference already exists";
                    return response;
                }
                //Save request
                var saved = DBConnect.SaveBillPaymentRequest(request);
                if (!saved)
                {
                    response.Status = false;
                    response.StatusMessage = "Unable to save request";
                    return response;
                }
                //Get Parameters from config
                string GL = ConfigurationManager.AppSettings["QuickTellerGL"];
                string TransactionCode = ConfigurationManager.AppSettings["TransactionCode"];
                string paymentAgentTable = ConfigurationManager.AppSettings["paymentAgentTable"];
                string NoDebit = ConfigurationManager.AppSettings["NoDebit"];
                decimal Fee = Convert.ToDecimal(ConfigurationManager.AppSettings["Fee"]);


                // Get Bill payment items
                var billPaymentItems = DBConnect.GetPaymentItems(request.BillerId);
                var billerdetails = DBConnect.GetBillerDetails(request.BillerId);
                var narration = billPaymentItems.Select(item => item.paymentCode == request.PaymentCode);
                if (!narration.Contains<bool>(true))
                {
                    response.Status = false;
                    response.StatusMessage = "Invalid payment code";
                    return response;
                }
                Fee = Convert.ToDecimal(billPaymentItems.FirstOrDefault(item => item.paymentCode == request.PaymentCode).itemFee);
                Fee = Fee > 0 ? Fee / 100 : Fee;
                //if (NoDebit.Contains(billerdetails.categoryid))
                //{
                //    Fee = 0;
                //}
                string sourceNarration = request.requestReference + "/" + billerdetails.billername + "/" + request.Amount;
                string destinationNarration = sourceNarration;

                //Debit Customer before calling Interswitch
                var tranresponse = PaymentAgent.PassSettlement(request.AccountNumber, GL, sourceNarration, destinationNarration, request.requestReference, request.Amount + Fee, TransactionCode, paymentAgentTable);
                if (!tranresponse.Status)
                {
                    response.Status = false;
                    response.StatusMessage = tranresponse.StatusMessage;
                    DBConnect.UpdateBillPaymentRequest(request.requestReference, 0, response.StatusMessage, null, null);
                    return response;
                }
                DBConnect.UpdateBillPaymentRequest(request.requestReference, 2, "Processing", tranresponse.UBSReference, null);
                using (HttpClient client = new HttpClient())
                {
                    var details = GetCustomerDetails(request.AccountNumber);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    string baseUrl = ConfigurationManager.AppSettings["baseUrl"];
                    var header = DBConnect.GenerateAPIHeader("POST", baseUrl + ApiUrl, "SHA1");

                    BillPaymentNotificationRequest interswitchRequest = new BillPaymentNotificationRequest()
                    {
                        amount = ((int)(request.Amount * 100)).ToString(),
                        customerEmail = details.Email,
                        customerId = request.CustomerId,
                        customerMobile = details.Phone,
                        paymentCode = request.PaymentCode,
                        requestReference = request.requestReference,
                        terminalId = header.TerminalID
                    };

                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("InterswitchAuth", header.Authorization);
                    client.DefaultRequestHeaders.Add("Signature", header.Signature);
                    client.DefaultRequestHeaders.Add("SignatureMethod", header.SignatureMethod);
                    client.DefaultRequestHeaders.Add("Timestamp", header.Timestamp);
                    client.DefaultRequestHeaders.Add("TerminalID", header.TerminalID);
                    client.DefaultRequestHeaders.Add("Nonce", header.Nonce);

                    // Serialize request
                    string json = Newtonsoft.Json.JsonConvert.SerializeObject(interswitchRequest);
                    var apirequest = new StringContent(json, Encoding.UTF8, "application/json");

                    // Post request to the API and get response
                    HttpResponseMessage result = client.PostAsync(ApiUrl, apirequest).Result;

                    if (result.IsSuccessStatusCode)
                    {
                        response = result.Content.ReadAsAsync<BillPaymentNotificationResponse>().Result;
                        if (result.StatusCode == HttpStatusCode.OK)
                        {
                            response.Status = true;
                            response.StatusMessage = "Approved and completed successfully";
                        }
                        else
                        {
                            var reversal = PaymentAgent.PassReversal2(request.requestReference, paymentAgentTable);
                            var errorMsg = result.Content.ReadAsAsync<ErrorResponse>().Result;
                            response.Status = false;
                            response.StatusMessage = errorMsg.error.message;
                        }
                        DBConnect.UpdateBillPaymentRequest(request.requestReference, 3, response.StatusMessage, null, response.transactionRef);
                    }
                    else
                    {
                        var reversal = PaymentAgent.PassReversal2(request.requestReference, paymentAgentTable);
                        var errorMsg = result.Content.ReadAsAsync<ErrorResponse>().Result;
                        response.Status = false;
                        response.StatusMessage = errorMsg.error.message;
                        DBConnect.UpdateBillPaymentRequest(request.requestReference, 0, response.StatusMessage, null, null);
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response.Status = false;
                response.StatusMessage = "Error connecting to Interswitch";
            }
            return response;
        }

        public static BillPaymentTransactionResponse SendBillPaymentTransaction(BillPaymentTransactionRequest request)
        {
            BillPaymentTransactionResponse response = new BillPaymentTransactionResponse();
            string ApiUrl = $"api/v2/quickteller/transactions";

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    string baseUrl = ConfigurationManager.AppSettings["baseUrl"];
                    var header = DBConnect.GenerateAPIHeader("POST", baseUrl + ApiUrl, "SHA1");

                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("InterswitchAuth", header.Authorization);
                    client.DefaultRequestHeaders.Add("Signature", header.Signature);
                    client.DefaultRequestHeaders.Add("SignatureMethod", header.SignatureMethod);
                    client.DefaultRequestHeaders.Add("Timestamp", header.Timestamp);
                    client.DefaultRequestHeaders.Add("TerminalID", header.TerminalID);
                    client.DefaultRequestHeaders.Add("Nonce", header.Nonce);

                    // Serialize request
                    string json = Newtonsoft.Json.JsonConvert.SerializeObject(request);
                    var apirequest = new StringContent(json, Encoding.UTF8, "application/json");

                    // Post request to the API and get response
                    HttpResponseMessage result = client.PostAsync(ApiUrl, apirequest).Result;

                    if (result.IsSuccessStatusCode)
                    {
                        response = result.Content.ReadAsAsync<BillPaymentTransactionResponse>().Result;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return response;
        }
        
        public static CustomerDetails GetCustomerDetails(string AccountNumber)
        {
            CustomerDetails result = new CustomerDetails();
            string ApiUrl = $"api/GetCustomerDetails/AccountNumber/{AccountNumber}";
            string BaseUrl = ConfigurationManager.AppSettings["CustomerAPI"];
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);

                    HttpResponseMessage response = client.GetAsync(ApiUrl).Result;
                    if (response.IsSuccessStatusCode && response.Content != null)
                    {
                        result = response.Content.ReadAsAsync<CustomerDetails>().Result;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        */
    }
}
