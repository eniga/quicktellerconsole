﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace QuickTellerConsole.Utilities
{
    public static class SHAUtility
    {
        public static string GenerateSHA256String(string input)
        {
            var result = "";
            byte[] data = Encoding.UTF8.GetBytes(input);
            using (SHA256 shaM = new SHA256Managed())
            {
                var hash = shaM.ComputeHash(data);
                foreach (byte x in hash)
                {
                    result += String.Format("{0:x2}", x);
                }
            }
            return result;
        }

        public static string GenerateSHA512String(string input)
        {
            var result = "";
            byte[] data = Encoding.UTF8.GetBytes(input);
            using (SHA512 shaM = new SHA512Managed())
            {
                var hash = shaM.ComputeHash(data);
                foreach (byte x in hash)
                {
                    result += String.Format("{0:x2}", x);
                }
            }
            return result;
        }

        public static string GenerateSHA1String(string input)
        {
            var result = "";
            byte[] data = Encoding.UTF8.GetBytes(input);
            using (SHA1 shaM = new SHA1Managed())
            {
                var hash = shaM.ComputeHash(data);
                foreach (byte x in hash)
                {
                    result += String.Format("{0:x2}", x);
                }
            }
            return result;
        }

        public static string SHA1Hash(string input)
        {
            var hash = (new SHA1CryptoServiceProvider()).ComputeHash(Encoding.UTF8.GetBytes(input));
            string result = Convert.ToBase64String(hash);
            return result;
        }

        public static string SHA256Hash(string input)
        {
            var hash = (new SHA256CryptoServiceProvider()).ComputeHash(Encoding.UTF8.GetBytes(input));
            string result = Convert.ToBase64String(hash);
            return result;
        }

        public static string SHA512Hash(string input)
        {
            var hash = (new SHA512CryptoServiceProvider()).ComputeHash(Encoding.UTF8.GetBytes(input));
            string result = Convert.ToBase64String(hash);
            return result;
        }
    }
}
