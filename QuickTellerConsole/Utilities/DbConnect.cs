﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerConsole.Models;
using QuickTellerConsole.Repositories;
using QuickTellerConsole.Services;

namespace QuickTellerConsole.Utilities
{
    public static class DbConnect
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        static CategoryService categoryService = new CategoryService();
        static BillerService billerService = new BillerService();
        static PaymentItemService paymentItemService = new PaymentItemService();
        static QTResponseService qtResponseService = new QTResponseService();

        public static async Task<QTResponse> GetQTResponseAsync(string Code)
        {
            return await qtResponseService.GetSingleAsync(Code);
        }

        public static async Task SaveBillersAsync(List<BillerDetails> billers)
        {
            await billerService.InsertBulk(billers);
            Console.WriteLine("Biller successfully saved to DB");

            List<pageFlowInfo> infos = new List<pageFlowInfo>();
            foreach(var item in billers)
            {
                pageFlowInfo page = item.pageFlowInfo;
                if (page != null)
                {
                    page.billerid = item.billerid;
                    infos.Add(page);
                }
            }
            await SavePageFlowInfoAsync(infos);
        }

        public static async Task SavePaymentItemsAsync(List<Paymentitem> items)
        {
            await paymentItemService.InsertBulk(items);
            Console.WriteLine("Payment Item successfully saved to DB");
        }

        public static async Task SaveCategoriesAsync(List<Category> categories)
        {
            await categoryService.InsertBulk(categories);
            Console.WriteLine("Biller Categories successfully saved to DB");
        }

        public static async Task SavePageFlowInfoAsync(List<pageFlowInfo> infos)
        {
            await billerService.InsertPageFlowInfoBulk(infos);
            Console.WriteLine("Page Flow Info successfully saved to DB");
        }
    }
}
