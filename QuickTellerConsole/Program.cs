﻿using System;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using QuickTellerConsole.Services;

namespace QuickTellerConsole
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if (!DEBUG)

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                    new Agent() 
                };
                ServiceBase.Run(ServicesToRun);

#else

            // Debug code: this allows the process to run as a non-service.
            // It will kick off the service start point, but never kill it.
            // Shut down the debugger to exit
            Agent service = new Agent();
            service.dummyStart();
            // Put a breakpoint on the following line to always catch
            // your service when it has finished its work
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);

#endif
        }
    }
}
