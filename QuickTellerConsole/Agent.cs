﻿using QuickTellerConsole.Services;
using System;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;

namespace QuickTellerConsole
{
    public class Agent : ServiceBase
    {
        public System.Timers.Timer thisTimer;
        public bool IsRunning;
        public string serviceDesc = "Quickteller.Console";

        public Agent()
        {
            //dummyStart();
        }

        public void dummyStart()
        {
            //SyncService.SyncBillerAsync(2).GetAwaiter();
            SyncService.Sync().GetAwaiter();
            //thisTimer = new System.Timers.Timer();
            //thisTimer.Enabled = true;
            //int timerInterval = 0;
            //bool frequencyOk = int.TryParse(ConfigurationManager.AppSettings["CheckDefaultFrequency"].ToString(), out timerInterval);
            //if (!frequencyOk)
            //{
            //    //unable to load from config file, proceed
            //    //logger.Info("Unable to load Default Frequency from web config @ Start, defaulting to 10800");
            //    timerInterval = 10800;
            //}
            //thisTimer.Interval = timerInterval;
            //thisTimer.AutoReset = true;
            //thisTimer.Elapsed += thisTimer_Tick;
            //thisTimer.Start();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            thisTimer = new System.Timers.Timer();
            thisTimer.Enabled = true;
            int timerInterval = 0;
            bool frequencyOk = int.TryParse(ConfigurationManager.AppSettings["CheckDefaultFrequency"].ToString(), out timerInterval);
            if (!frequencyOk)
            {
                //un-able to load from config file, proceed
                //logger.Info("Unable to load Default Frequency from web config @ Start, defaulting to 10800");
                timerInterval = 10800;
            }
            //logger.Info("Agent started");
            thisTimer.Interval = timerInterval;
            thisTimer.AutoReset = true;
            thisTimer.Elapsed += thisTimer_Tick;
            thisTimer.Start();
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            //logger.Info("Agent Stopped.");
        }

        private void thisTimer_Tick(System.Object sender, System.EventArgs e)
        {
            DoNextExecutionAsync();
        }

        public void DoNextExecutionAsync()
        {
            lock (this)
            {
                thisTimer.Stop();
                processNextTransaction();

                int timerInterval = 0;
                bool frequencyOk = int.TryParse(ConfigurationManager.AppSettings["CheckDefaultFrequency"], out timerInterval);
                if (!frequencyOk)
                {
                    //un-able to load from config file, proceed
                    //logger.Info("Unable to load Default Frequency from web config @ Start, defaulting to 10800");
                    timerInterval = 10800;
                }
                thisTimer.Interval = timerInterval;
                thisTimer.Start();
            }
        }

        public void processNextTransaction()
        {
            SyncService.Sync().GetAwaiter();
            //while (true)
            //{

            //    var now = DateTime.Now;
            //    var tomorrow = now.AddDays(1);
            //    var durationUntilMidnight = tomorrow.Date - now;

            //    var t = new Timer(async o =>
            //    {
            //        Console.WriteLine(string.Format("Starting {0} at {1}", ServiceName, DateTime.Now));
            //        if (!RuntimeLogService.HasRun())
            //        {
            //            await SyncService.Sync();
            //            RuntimeLogService.AddLog();
            //        }
            //    }, null, TimeSpan.FromDays(1), durationUntilMidnight);
            //}
        }
    }
}
