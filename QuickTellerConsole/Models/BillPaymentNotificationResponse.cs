﻿using System;
namespace QuickTellerConsole.Models
{
    public class BillPaymentNotificationResponse
    {
        public bool Status { get; set; }
        public string StatusMessage { get; set; }
        public string transactionRef { get; set; }
    }
}
