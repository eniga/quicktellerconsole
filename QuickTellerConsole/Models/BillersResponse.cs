﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Dapper;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace QuickTellerConsole.Models
{
    public class BillersResponse
    {
        public List<BillerDetails> billers { get; set; }
    }

    [Table("tbl_qt_billers")]
    public class BillerDetails
    {
        public string categoryid { get; set; }
        public string categoryname { get; set; }
        public string categorydescription { get; set; }
        [Key, Required]
        public string billerid { get; set; }
        public string billername { get; set; }
        public string customerfield1 { get; set; }
        public string supportemail { get; set; }
        public string paydirectProductId { get; set; }
        public string paydirectInstitutionId { get; set; }
        public string narration { get; set; }
        public string shortName { get; set; }
        public string surcharge { get; set; }
        public string currencyCode { get; set; }
        public string quickTellerSiteUrlName { get; set; }
        public string smallImageId { get; set; }
        public string amountType { get; set; }
        [LeftJoin("tbl_qt_pageflowinfo", "billerid", "billerid")]
        public pageFlowInfo pageFlowInfo { get; set; }
        public string currencySymbol { get; set; }
        public string logoUrl { get; set; }
        public string networkId { get; set; }
        public string productCode { get; set; }
        public string type { get; set; }
    }

    [Table("tbl_qt_pageflowinfo")]
    public class pageFlowInfo
    {
        [Key, Required]
        public string billerid { get; set; }
        public bool allowRetry { get; set; }
        public string finishButtonName { get; set; }
        public bool performInquiry { get; set; }
        public string startPage { get; set; }
        public bool usesPaymentItems { get; set; }
    }
}
