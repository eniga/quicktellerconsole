﻿using System;
namespace QuickTellerConsole.Models
{
    public class QueryTransactionResponse
    {
        public TransactionDetails billPayment { get; set; }
    }

    public class TransactionDetails
    {
        public TransactionBillerDetails billerDetails { get; set; }
        public string amount { get; set; }
        public string currencyCode { get; set; }
        public string customer { get; set; }
        public string customerEmail { get; set; }
        public string customerMobile { get; set; }
        public string paymentDate { get; set; }
        public string requestReference { get; set; }
        public string serviceCode { get; set; }
        public string serviceName { get; set; }
        public string serviceProviderId { get; set; }
        public string status { get; set; }
        public string surcharge { get; set; }
        public string transactionRef { get; set; }
        public string transactionResponseCode { get; set; }
        public string transactionSet { get; set; }
    }

    public class TransactionBillerDetails
    {
        public string biller { get; set; }
        public string customerId1 { get; set; }
        public string customerId2 { get; set; }
        public string paymentTypeName { get; set; }
        public string paymentTypeCode { get; set; }
        public string billerId { get; set; }
    }
}
