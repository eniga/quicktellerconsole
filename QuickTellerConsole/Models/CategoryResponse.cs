﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTellerConsole.Models
{
    [Table("tbl_qt_categories")]
    public class Category
    {
        [Key, Required]
        public string categoryid { get; set; }
        public string categoryname { get; set; }
        public string categorydescription { get; set; }
    }

    public class CategoryResponse
    {
        public List<Category> categorys { get; set; }
    }
}
