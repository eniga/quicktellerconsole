﻿using System;
namespace QuickTellerConsole.Models
{
    public class APIHeader
    {
        public string Timestamp { get; set; }
        public string Nonce { get; set; }
        public string Authorization { get; set; }
        public string Signature { get; set; }
        public string SignatureMethod { get; set; }
        public string TerminalID { get; set; }
        public string ClientID { get; set; }
        public string RequestReferencePrefix { get; set; }
        public string Secret { get; set; }
    }
}
