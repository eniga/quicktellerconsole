﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTellerConsole.Models
{
    [Table("tbl_qt_response")]
    public class QTResponse
    {
        [Key, Required]
        public string Code { get; set; }
        public string Description { get; set; }
        public string Resolution { get; set; }
        public bool Status { get; set; }
    }
}
