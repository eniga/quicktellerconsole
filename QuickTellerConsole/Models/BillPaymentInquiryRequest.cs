﻿using System;
namespace QuickTellerConsole.Models
{
    public class BillPaymentInquiryRequest
    {
        public string paymentCode { get; set; }
        public string customerId { get; set; }
        public string customerMobile { get; set; }
        public string customerEmail { get; set; }
    }
}
