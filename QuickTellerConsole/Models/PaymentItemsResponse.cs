﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTellerConsole.Models
{
    public class PaymentItemsResponse
    {
        public List<Paymentitem> paymentitems { get; set; }
    }

    [Table("tbl_qt_paymentitems")]
    public class Paymentitem
    {
        public string categoryid { get; set; }
        public string billerid { get; set; }
        public bool isAmountFixed { get; set; }
        [Key, Required]
        public string paymentitemid { get; set; }
        public string paymentitemname { get; set; }
        public string amount { get; set; }
        public string code { get; set; }
        public string currencyCode { get; set; }
        public string currencySymbol { get; set; }
        public string itemCurrencySymbol { get; set; }
        public string sortOrder { get; set; }
        public string pictureId { get; set; }
        public string paymentCode { get; set; }
        public string itemFee { get; set; }
    }
}
