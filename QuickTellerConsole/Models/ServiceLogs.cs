﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTellerConsole.Models
{
    [Table("tbl_qt_logs")]
    public class ServiceLogs
    {
        [Key]
        public int Id { get; set; }
        public DateTime RunDate { get; set; }
        public bool Status { get; set; }
    }
}
