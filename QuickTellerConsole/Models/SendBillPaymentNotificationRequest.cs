﻿using System;
namespace QuickTellerConsole.Models
{
    public class SendBillPaymentNotificationRequest
    {
        public string BillerId { get; set; }
        public string PaymentCode { get; set; }
        public string CustomerId { get; set; }
        public string AccountNumber { get; set; }
        public decimal Amount { get; set; }
        public string requestReference { get; set; }
    }

    public class DBBillPayment
    {
        public string RequestReference { get; set; }
        public string BillerId { get; set; }
        public string TerminalId { get; set; }
        public string PaymentCode { get; set; }
        public string RequestType { get; set; }
        public int ChannelId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerMobile { get; set; }
        public string CustomerEmail { get; set; }
        public decimal Amount { get; set; }
        public DateTime DateSubmitted { get; set; }
        public string Status { get; set; }
        public string StatusMessage { get; set; }
        public string TransactionRef { get; set; }
        public DateTime LastUpdated { get; set; }
    }
}
