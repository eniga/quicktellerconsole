﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerConsole.Models;
using QuickTellerConsole.Repositories;

namespace QuickTellerConsole.Services
{
    public class CategoryService
    {
        CategoryRepository repo;
        string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public CategoryService()
        {
            var conn = new SqlConnection(connectionString);
            var generator = new SqlGenerator<Category>(SqlProvider.MSSQL);
            repo = new CategoryRepository(conn, generator);
        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            return await repo.FindAllAsync();
        }

        public async Task<Category> GetSingleAsync(string CategoryId)
        {
            return await repo.FindAsync(x => x.categoryid == CategoryId);
        }

        public async Task<bool> UpdateSingleAsync(Category category)
        {
            return await repo.UpdateAsync(category);
        }

        public async Task<bool> UpdateBulk(List<Category> categories)
        {
            return await repo.BulkUpdateAsync(categories);
        }

        public async Task<bool> InsertSingle(Category category)
        {
            try
            {
                return repo.Insert(category);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Insert category error {e.Message}");
                return false;
            }
            //return await repo.InsertAsync(category);
        }

        public async Task<int> InsertBulk(List<Category> categories)
        {
            int inserted = 0;

            foreach (var item in categories)
            {
                if(await InsertSingle(item))
                {
                    inserted++;
                }
            }

            return inserted;
            //return await repo.BulkInsertAsync(categories);
        }

        public async Task<bool> DeleteSingle(Category category)
        {
            return await repo.DeleteAsync(category);
        }
    }
}
