﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerConsole.Models;
using QuickTellerConsole.Repositories;

namespace QuickTellerConsole.Services
{
    public class PaymentItemService
    {
        PaymentItemRepository repo;
        string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public PaymentItemService()
        {
            var conn = new SqlConnection(connectionString);
            var generator = new SqlGenerator<Paymentitem>(SqlProvider.MSSQL);
            repo = new PaymentItemRepository(conn, generator);
        }

        public async Task<IEnumerable<Paymentitem>> GetAllAsync()
        {
            return await repo.FindAllAsync();
        }

        public async Task<Paymentitem> GetSingleAsync(string paymentitemid)
        {
            return await repo.FindAsync(x => x.paymentitemid == paymentitemid);
        }

        public async Task<bool> UpdateSingleAsync(Paymentitem paymentitem)
        {
            return await repo.UpdateAsync(paymentitem);
        }

        public async Task<bool> UpdateBulk(List<Paymentitem> paymentitems)
        {
            return await repo.BulkUpdateAsync(paymentitems);
        }

        public async Task<bool> InsertSingle(Paymentitem paymentitem)
        {
            return await repo.InsertAsync(paymentitem);
        }

        public async Task<int> InsertBulk(List<Paymentitem> paymentitems)
        {
            try
            {
                int ins = repo.BulkInsert(paymentitems);
                return ins;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Insert payment item error {e.Message}");
                return 0;
            }
            //return await repo.BulkInsertAsync(paymentitems);
        }

        public async Task<bool> DeleteSingle(Paymentitem paymentitem)
        {
            return await repo.DeleteAsync(paymentitem);
        }
    }
}
