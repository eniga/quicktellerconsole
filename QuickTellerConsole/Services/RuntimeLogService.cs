﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using QuickTellerConsole.Models;

namespace QuickTellerConsole.Services
{
    public static class RuntimeLogService
    {
        static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public static void AddLog()
        {
            using(IDbConnection conn = new SqlConnection(ConnectionString))
            {
                ServiceLogs logs = new ServiceLogs() { RunDate = DateTime.Now, Status = true };
                conn.Execute("insert into tbl_qt_logs values(@RunDate, @Status)", logs);
            }
        }

        public static bool HasRun()
        {
            using (IDbConnection conn = new SqlConnection(ConnectionString))
            {
                var result = conn.Query<ServiceLogs>("select * from tbl_qt_logs where convert(varchar, RUNDATE, 103) = convert(varchar, getdate(), 103)").AsList();
                return result.Count > 0;
            }
        }
    }
}
