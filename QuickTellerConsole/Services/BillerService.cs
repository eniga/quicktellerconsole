﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerConsole.Models;
using QuickTellerConsole.Repositories;

namespace QuickTellerConsole.Services
{
    public class BillerService
    {
        BillerRepository repo;
        PageFlowInfoRepository pageFlowInfoRepository;
        string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public BillerService()
        {
            var conn = new SqlConnection(connectionString);
            var generator = new SqlGenerator<BillerDetails>(SqlProvider.MSSQL);
            var generator2 = new SqlGenerator<pageFlowInfo>(SqlProvider.MSSQL);
            repo = new BillerRepository(conn, generator);
            pageFlowInfoRepository = new PageFlowInfoRepository(conn, generator2);
        }

        public async Task<IEnumerable<BillerDetails>> GetAllAsync()
        {
            return await repo.FindAllAsync();
        }

        public async Task<BillerDetails> GetSingleAsync(string BillerId)
        {
            return await repo.FindAsync(x => x.billerid == BillerId);
        }

        public async Task<bool> UpdateSingleAsync(BillerDetails biller)
        {
            return await repo.UpdateAsync(biller);
        }

        public async Task<bool> UpdateBulk(List<BillerDetails> billers)
        {
            return await repo.BulkUpdateAsync(billers);
        }

        public async Task<bool> InsertSingle(BillerDetails biller)
        {
            try
            {
                return repo.Insert(biller);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error while inserting single item {e.Message}");
                //throw;
                return false;
            }
            //return await repo.InsertAsync(biller);
        }

        public async Task<int> InsertBulk(List<BillerDetails> billers)
        {
            int inserted = 0;
            try
            {
                foreach (var item in billers)
                {
                    if(await InsertSingle(item))
                    {
                        inserted++;
                    }
                }
                //int inserted = await repo.BulkInsertAsync(billers);
                return inserted;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error while inserting single item {e.Message}");
                throw;
            }
             
        }

        public async Task<bool> DeleteSingle(BillerDetails biller)
        {
            return await repo.DeleteAsync(biller);
        }

        public async Task<bool> InsertPageFlowInfo(pageFlowInfo info)
        {
            try
            {
                return pageFlowInfoRepository.Insert(info);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Insert page flow error {e.Message}");
                return false;
            }
            //return await pageFlowInfoRepository.InsertAsync(info);
        }

        public async Task<int> InsertPageFlowInfoBulk(List<pageFlowInfo> infos)
        {
            int inserted = 0;

            foreach (var item in infos)
            {
                if(await InsertPageFlowInfo(item))
                {
                    inserted++;
                }
            }
            return inserted;
            //return await pageFlowInfoRepository.BulkInsertAsync(infos);
        }
    }
}
