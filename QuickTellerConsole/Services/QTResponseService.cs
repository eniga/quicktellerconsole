﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerConsole.Models;
using QuickTellerConsole.Repositories;

namespace QuickTellerConsole.Services
{
    public class QTResponseService
    {
        QTResponseRepository repo;
        string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public QTResponseService()
        {
            var conn = new SqlConnection(connectionString);
            var generator = new SqlGenerator<QTResponse>(SqlProvider.MSSQL);
            repo = new QTResponseRepository(conn, generator);
        }

        public async Task<IEnumerable<QTResponse>> GetAllAsync()
        {
            return await repo.FindAllAsync();
        }

        public async Task<QTResponse> GetSingleAsync(string Code)
        {
            return await repo.FindAsync(x => x.Code == Code);
        }
    }
}
