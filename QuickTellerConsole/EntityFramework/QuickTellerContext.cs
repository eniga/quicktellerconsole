﻿using Microsoft.EntityFrameworkCore;
using QuickTellerConsole.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickTellerConsole.EntityFramework
{
    public class QuickTellerContext : DbContext
    {
        public QuickTellerContext(DbContextOptions<QuickTellerContext> options):base(options)
        {

        }

        public DbSet<BillerDetails> BillerDetails { get; set; }
        public DbSet<pageFlowInfo> pageFlowInfo { get; set; }
        public DbSet<Paymentitem> Paymentitem { get; set; }
        public DbSet<Category> Category { get; set; }
    }
}
