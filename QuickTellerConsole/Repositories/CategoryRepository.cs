﻿using System;
using System.Configuration;
using System.Data;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerConsole.Models;

namespace QuickTellerConsole.Repositories
{
    public class CategoryRepository : DapperRepository<Category>
    {
        public CategoryRepository(IDbConnection connection, ISqlGenerator<Category> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}
