﻿using System;
using System.Configuration;
using System.Data;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerConsole.Models;

namespace QuickTellerConsole.Repositories
{
    public class PaymentItemRepository : DapperRepository<Paymentitem>
    {
        public PaymentItemRepository(IDbConnection connection, ISqlGenerator<Paymentitem> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}
