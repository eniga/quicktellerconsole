﻿using System;
using System.Configuration;
using System.Data;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerConsole.Models;

namespace QuickTellerConsole.Repositories
{
    public class PageFlowInfoRepository : DapperRepository<pageFlowInfo>
    {
        public PageFlowInfoRepository(IDbConnection connection, ISqlGenerator<pageFlowInfo> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}
