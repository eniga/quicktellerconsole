﻿using System;
using System.Data;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerConsole.Models;

namespace QuickTellerConsole.Repositories
{
    public class BillerRepository : DapperRepository<BillerDetails>
    {
        public BillerRepository(IDbConnection connection, ISqlGenerator<BillerDetails> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}
