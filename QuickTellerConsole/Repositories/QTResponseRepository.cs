﻿using System;
using System.Configuration;
using System.Data;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerConsole.Models;

namespace QuickTellerConsole.Repositories
{
    public class QTResponseRepository : DapperRepository<QTResponse>
    {
        public QTResponseRepository(IDbConnection connection, ISqlGenerator<QTResponse> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}
