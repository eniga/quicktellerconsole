﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using QuickTellerConsole.EntityFramework;
using QuickTellerConsole.Models;
using QuickTellerConsole.Services;
using QuickTellerConsole.Utilities;

namespace QuickTellerConsole
{
    public static class SyncService
    {
        static DbContextOptionsBuilder<QuickTellerContext> builder = null;
        static string connectionString = null;
        static QuickTellerContext context = null;

        static SyncService()
        {
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            builder = new DbContextOptionsBuilder<QuickTellerContext>();
            builder.UseSqlServer(connectionString);

            context = new QuickTellerContext(builder.Options);
        }
            
        public static async Task Sync()
        {
            await SyncBillersAsync();
            //await SyncCategories();
        }

        private static async Task SyncBillersAsync()
        {
            try
            {
                var response = Integration.GetBillers();

                //File.WriteAllText("Billers.txt", JsonConvert.SerializeObject(response));

                if (response.billers.Count > 0)
                {

                    try
                    {
                        List<BillerDetails> billers = response.billers;
                        List<pageFlowInfo> infos = new List<pageFlowInfo>();
                        List<BillerDetails> billers2 = new List<BillerDetails>();

                        billers.ForEach(biller =>
                        {
                            if (!context.BillerDetails.AnyAsync(b => b.billerid == biller.billerid).Result)
                            {
                                if(biller.billerid != null)
                                {

                                    if (string.IsNullOrWhiteSpace(biller.pageFlowInfo.billerid))
                                    {
                                        biller.pageFlowInfo.billerid = biller.billerid;
                                    }

                                    billers2.Add(biller);
                                    context.BillerDetails.Add(biller);
                                }
                                else
                                {

                                }

                                pageFlowInfo page = biller.pageFlowInfo;
                                if (page != null)
                                {
                                    page.billerid = biller.billerid;
                                    infos.Add(page);
                                }
                            }
                        });

                        context.SaveChanges();

                        if (infos != null)
                        {
                            infos.ForEach(i =>
                            {
                                if (!context.pageFlowInfo.AnyAsync(p => p.billerid == i.billerid).Result)
                                {
                                    context.pageFlowInfo.Add(i);

                                }
                            });
                        }

                        context.SaveChanges();

                        billers.ForEach(biller =>
                        {
                            SyncPaymentItemAsync(biller.billerid).GetAwaiter();
                        });
                    }
                    catch (Exception e)
                    {

                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static async Task SyncPaymentItemAsync(string billerid)
        {
            try
            {
                var response = Integration.GetBillerPaymentItems(billerid);
                if(response.paymentitems.Count > 0)
                {
                    List<Paymentitem> paymentitems = response.paymentitems;

                    paymentitems.ForEach(item =>
                    {
                        if (!context.Paymentitem.AnyAsync(p => p.paymentitemid == item.paymentitemid).Result)
                        {
                            context.Paymentitem.Add(item);
                        }
                    });

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static async Task SyncCategories()
         {
            try
            {
                var response = Integration.GetBillerCategories();
                if (response.categorys.Count > 0)
                {
                    List<Category> categories = response.categorys;

                    categories.ForEach(async c =>
                    {
                        if(!await context.Category.AnyAsync(i => i.categoryid == c.categoryid))
                        {
                            context.Category.Add(c);
                        }
                    });

                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static async Task SyncBillerAsync(int id)
        {
            try
            {
                var response = Integration.GetBillersByCategory(id);
                if (response.billers.Count > 0)
                {
                    List<BillerDetails> billers = response.billers;

                    //Save all billers
                    await DbConnect.SaveBillersAsync(billers);

                    //Get and save payment item for each biller

                    billers.ForEach(biller =>
                    {
                        SyncPaymentItemAsync(biller.billerid).GetAwaiter();
                    });
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
